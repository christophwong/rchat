# rChat (a demo of actionCable)

rChat is a simple chat application that harnesses the power of ActionCable,
 a WebSocket library introduced in Rails 5.

![User journey for simple chat](./rchat-heroku-test.gif)

## Live Demo site 

(note: it is deployed on Heroku's free tier, so it may be slow when accessing it for the first time)

https://rchat-action-cable-demo.herokuapp.com

## Local Setup

#### 0. Check in versions and dependencies are installed

#### 1. Clone the repo

`git clone git@gitlab.com:christophwong/rchat.git`

#### 2. Setup

2.1. Gems

`bundle install`

2.2. Database migration

`rails db:create`

`rails db:migrate`

#### 3. Run app

`rails server`

## Versions and Dependencies

Ruby - v 2.5.3

Rails - v 5.2.1

Postgres - v 9.3.24

Redis - v 4.0.3

Bundler - v 1.17.1

## Test

`rails test`
