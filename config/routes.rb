Rails.application.routes.draw do
  root 'conversations#index'

  get 'signup', to: 'users#new', as: 'signup'
  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  delete 'logout', to: 'sessions#destroy'

  resources :users
  resources :private_messages, only: [:create]
  resources :conversations, only: [:index, :show, :new]

# config.action_cable.mount_path = "/cable #might need this?
  mount ActionCable.server => '/cable'
end
