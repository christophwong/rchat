class ConversationsController < ApplicationController
  def index
    if (helpers.logged_in?)
      @conversations = Conversation.participating(helpers.current_user).order('updated_at DESC')
      redirect_to users_path if @conversations.empty?
    else
      redirect_to login_path
    end
  end

  def show
    @conversation = Conversation.find_by(id: params[:id])
    redirect_to root_path unless @conversation && @conversation.participates?(helpers.current_user)
  end
  
  def new
    if @recipient = User.find_by(id: params[:recipient_id])
      @conversation = Conversation.between(helpers.current_user.id, @recipient.id)[0] ||
      Conversation.create(author_id: helpers.current_user.id, recipient_id: @recipient.id)
      redirect_to conversation_path(@conversation)
    else
      flash[:error] = "Recipient does not exist"
      redirect_to root_path
    end
  end

  def create
  end

  def destroy
  end
end
