class PrivateMessagesController < ApplicationController
  before_action :find_conversation!

  def create
    @conversation
    @private_message = helpers.current_user.private_messages.build(private_message_params)
    @private_message.conversation_id = @conversation.id
    unless @private_message.save!
      flash[:error] = "Something went wrong, try again"
    end
  end

  private

  def private_message_params
    params.require(:private_message).permit(:body)
  end

  def find_conversation!
      @conversation = Conversation.find_by(id: params[:conversation_id])
      flash[:error] = 'no conversation'
      redirect_to(root_path) and return unless @conversation && @conversation.participates?(helpers.current_user)
  end
end