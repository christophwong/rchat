class UsersController < ApplicationController
  def new
    @user = User.new
  end

  def index
    @users = User.all
  end

  def show
    if helpers.logged_in?
      render 'show'
    else
      redirect_to login_url
    end
  end

  def create
    @user = User.new(user_params)
    if @user.save
      helpers.log_in(@user)
      redirect_to users_path
    else
      render 'new'
    end
  end

  private

  def user_params
    params.require(:user).permit(:email, :password)
  end
end
