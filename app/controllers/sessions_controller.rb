class SessionsController < ApplicationController
  def new
  end

  def create
    is_guest_login = params[:session][:guest]
    if is_guest_login
      create_and_log_in_as_guest
    else
      user = User.find_by(email: params[:session][:email].downcase)
            .try(:authenticate, params[:session][:password]) #maybe we can move this into an auth module, since logging in is always paired with authentication or account creation.
      if user
        helpers.log_in(user)
        redirect_to root_path
      else
        flash.now[:notice] = "Login information is incorrect"
        render 'new'
      end
    end
  end

  def destroy
    helpers.log_out
    redirect_to root_url
  end

  private

  def create_and_log_in_as_guest
    random_string = SecureRandom.hex(10)
    guest_user = User.new(email: "guest-#{random_string}@temp.com", password: random_string)
    
    if guest_user.save
      helpers.log_in(guest_user)
      redirect_to root_path
    else
      flash.now[:error] = "Problem creating guest user"
      render 'new'
    end
  end
end
