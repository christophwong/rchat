module SessionsHelper
  def log_in(authenticated_user)
    cookies.signed[:user_id] ||= authenticated_user.id
    @current_user = authenticated_user
  end

  def is_current_user?(user)
    user.id == current_user.id
  end

  def current_user
    if cookies.signed[:user_id]
      @current_user ||= User.find_by(id: cookies.signed[:user_id])
    end
  end

  def logged_in?
    !current_user.nil?
  end

  def ensure_logged_in
    unless logged_in?
      redirect_to(login_path)
    end    
  end

  def log_out
    cookies.delete(:user_id)
    @current_user = nil
  end
end
