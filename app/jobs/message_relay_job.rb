class MessageRelayJob < ApplicationJob
  def perform(message)
    author = message.conversation.author
    recipient = message.conversation.recipient
  #TODO: just need to broadcast to message recipient.
    broadcast(message, author) #stop broadcast to author, use ajax to self instead.
    broadcast(message, recipient)
  end

  def broadcast(message, subscriber)
    ActionCable.server.broadcast "chats_#{subscriber.id}_channel",
    message: PrivateMessagesController.render(message)
  end
end