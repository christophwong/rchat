class PrivateMessage < ApplicationRecord
  belongs_to :conversation
  belongs_to :user
  validates :body, presence: true

  after_commit { MessageRelayJob.perform_now(self) }
end
