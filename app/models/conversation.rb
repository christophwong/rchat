class Conversation < ApplicationRecord
  belongs_to :author, class_name: 'User'
  belongs_to :recipient, class_name: 'User'
  has_many :private_messages, -> { order(created_at: :asc) }, dependent: :destroy
  validates :author, uniqueness: {scope: :recipient}

  scope :participating, -> (user) do
    where("(conversations.author_id = ? OR conversations.recipient_id = ?)", user.id, user.id)
  end

  scope :between, -> (sender_id, recipient_id) do
    where(author_id: sender_id, recipient_id: recipient_id).or(where(author_id: recipient_id, recipient_id: sender_id))
end

  def with(current_user)
    author == current_user ? recipient : author
  end

  def participates?(user)
    author == user || recipient == user
  end

end
