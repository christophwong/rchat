class User < ApplicationRecord
  has_many :authored_conversations, class_name: 'Conversation', foreign_key: 'author_id'
  has_many :received_conversations, class_name: 'Conversation', foreign_key: 'recipient_id'
  has_many :private_messages, dependent: :destroy
  before_save {email.downcase!}
  has_secure_password
  validates :email,
   presence: true,
   uniqueness: {case_sensitive: false}

   def name
    email.split('@')[0]
  end
end
