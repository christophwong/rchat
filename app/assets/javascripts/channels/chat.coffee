scrollToBottom = ->
  if messages = document.querySelector '#conversation-body'
    messages.scrollTop = messages.scrollHeight

findAuthors = ->
  currentUserId = document.querySelector('#uid-ref').dataset.uid
  messages = document.querySelectorAll('.private_message')
  if messages
    messages.forEach(
      (message)->
        isAuthor = message.dataset.uid == currentUserId;
        className = 'is_author'
        if (isAuthor)
          if (message.classList)
            message.classList.add(className)
          else
            message.className += ' ' + className
        )

refresh = ->
  scrollToBottom()
  findAuthors()

App.chat = App.cable.subscriptions.create {channel: "ChatChannel", user: 1},
  connected: ->
    refresh()

  disconnected: ->

  received: (data) ->
    if messages = document.querySelector '#conversation-body'
      messages.insertAdjacentHTML('beforeend', data.message)
      refresh()