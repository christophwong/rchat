// Although this is also in channel code, on page load it flickers so this is for inital load

document.addEventListener('turbolinks:load', function(){
  const currentUserId = document.querySelector('#uid-ref').dataset.uid

  messages = document.querySelectorAll('.private_message')

  messages.forEach( message => {
    isAuthor = message.dataset.uid == currentUserId;
    className = 'is_author';
    if (isAuthor) {
      if (message.classList){
        message.classList.add(className)}
        else{
          message.className += ' ' + className
        }
      }
    })


  let sendButtons = document.querySelector('#js-send-form');
  if(sendButtons){
    sendButtons.addEventListener('keydown', e => {
      if (e.keyCode == 13){
        e.preventDefault();
        Rails.fire(e.target.form, 'submit')
        e.target.form.reset(); 
      }
    })
  }

  if(messages = document.querySelector('#conversation-body')) {
    messages.scrollTop = messages.scrollHeight
  }

})
