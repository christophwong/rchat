require 'test_helper'

class PrivateMessageTest < ActiveSupport::TestCase
  def setup
    @user = User.create(email: "test string", password: "any string right now")
    @conversation = Conversation.create(author: @user, recipient: @user)
    @body_text = "Some law rum lip sem"
  end

  test "valid Private message has conversation" do
    message = PrivateMessage.create

    message.conversation = @conversation
    message.user = @user
    message.body = @body_text

    assert message.valid?
  end

  test "calls message relay job after commit" do
    message = PrivateMessage.new

    message.conversation = @conversation
    message.user = @user
    message.body = @body_text
  end
end
