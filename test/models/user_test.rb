  require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(email: "test string", password: "any string right now")
  end

  test "valid user" do
    assert @user.valid?
  end

  test 'user needs email' do
    @user.email = ""
    refute @user.valid?
  end

  test 'user must be unique' do
    dup_user = @user.dup
    @user.email.upcase!
    @user.save
    refute dup_user.valid?
  end

  test 'email should be saved as all lowercase' do
    mix_case_email = "MiX-cASe@test.com"
    @user.email = mix_case_email
    @user.save
    assert_equal mix_case_email.downcase, @user.reload.email
  end
end
