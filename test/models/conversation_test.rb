require 'test_helper'

class ConversationTest < ActiveSupport::TestCase
  def setup
    @user1 = User.new
    @user2 = User.new
  end

  test 'valid conversation has author and recipient' do
    c = Conversation.new(author: @user1, recipient: @user2)
    assert c.valid?
  end
end
