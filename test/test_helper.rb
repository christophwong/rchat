ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
  def default_test_password
    'testpassword'
  end

  def is_logged_in?
    !cookies[:user_id].nil?
  end

  def login
    login_as(users(:one))
  end

  def login_as(user)
    post login_path, params: { session: {email: user.email, password: default_test_password }}
  end
end
