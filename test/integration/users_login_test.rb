require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
  test "user login with incorrect info should see flash message" do
    get login_path
    assert_template 'sessions/new'
    post login_path, params: { session: { email: "", password: "" } }
    assert_template 'sessions/new'
    assert_equal "Login information is incorrect", flash.notice
    get root_path
    assert flash.empty?
  end

  test 'user login redirects to home page' do
    login
    follow_redirect!
    assert is_logged_in?
    assert_equal root_path, path
  end
end
