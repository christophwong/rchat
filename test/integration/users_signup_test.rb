require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  test "user sign up logs user in as well" do
    get signup_path
    refute is_logged_in?

    assert_difference "User.count", 1 do
      r = post users_path, params: { user: {email: 'test@test.com', password: 'testpassword' }}
    end

    follow_redirect!
    assert is_logged_in?
    assert_match %r(users\Z), path
  end
end
