require 'test_helper'

class GuestsLoginTest < ActionDispatch::IntegrationTest
  test "guest user login" do
    get login_path
    assert_template 'sessions/new'
    post login_path, params: { session: { guest: true } }
    follow_redirect!
    assert is_logged_in?
  end
end
