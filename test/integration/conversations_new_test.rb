require 'test_helper'

class ConversationsNewTest < ActionDispatch::IntegrationTest
  test "redirects to users path when user has no participating conversations" do
    login
    get root_path
    assert_redirected_to users_path
  end

  test 'user sees conversations on home page' do
    login_as(users(:author))
    get root_path
    assert_select "#conversations"
  end

  test 'conversation shows private messages' do
    login_as(users(:author))
    get conversation_path(conversations(:testConversation))
    assert_match "/conversations/#{conversations(:testConversation).id}", path
    assert_select('#conversation-body') do
      assert_select '.show-message-body' do |messages|
        assert_equal messages[0].text, private_messages(:two).body
        assert_equal messages[1].text, private_messages(:three).body
      end
    end
  end

  test 'shows send message form' do
    login_as(users(:author))
    get conversation_path(conversations(:testConversation))
    assert_select '#js-send-form' do |form_el|
      assert_equal 'post', form_el.attr('method').value
      assert_equal '/private_messages', form_el.attr('action').value
      assert_equal 'true', form_el.attr('data-remote').value
    end
  end

  test 'sending message to another user' do
    message_body_text = "testing 1 2 3."
    login_as(users(:author))

    assert_difference "PrivateMessage.count", 1 do
      post private_messages_path, params:
      {
        conversation_id: conversations(:testConversation).id,
        private_message: {
          body: message_body_text
        }
      }, xhr: true
    end

    assert_response :success
    assert_equal "text/javascript", @response.content_type
    assert_match message_body_text, @response.body
  end

  test 'shows existing conversation when starting conversation with user' do
    login_as(users(:author))
    get new_conversation_path(recipient_id: users(:recipient).id)
    follow_redirect!
    assert_response :success
    assert_match "/conversations/#{conversations(:testConversation).id}", path
  end

  test 'redirects to root with flash error when recipient does not exist for new conversation' do
    login_as(users(:author))
    get new_conversation_path(recipient_id: (users(:recipient).id + 1))
    follow_redirect!
    assert_response :success
    assert_equal "Recipient does not exist", flash[:error]
    assert_match root_path, path
  end



end
